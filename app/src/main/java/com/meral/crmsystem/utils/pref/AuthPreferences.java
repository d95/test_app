package com.meral.crmsystem.utils.pref;


import android.content.Context;
import android.content.SharedPreferences;

import com.meral.crmsystem.activitis.launch.data.LoginData;

import javax.inject.Inject;

public class AuthPreferences implements AuthPreferencesHelper {
    private static final String AUTH_KEY_SHARED_PREFERENCES_LOGIN_DATA = "LOGIN_DATA";

    public static final String AUTH_KEY_LOGIN = "login";
    public static final String AUTH_KEY_PASSWORD = "password";
    public static final String AUTH_KEY_IS_SAVE_LOGIN_DATA= "is_save_login_data";

    private final SharedPreferences preferences;

    @Inject
    public AuthPreferences(Context context) {
        preferences = context.getSharedPreferences(AUTH_KEY_IS_SAVE_LOGIN_DATA, Context.MODE_PRIVATE);
    }

    public void putCurrentLoginData(LoginData loginData) {
        String login = loginData.getLogin();
        String password = loginData.getPassword();

        putCurrentLoginData(login, password);
    }

    private void putCurrentLoginData(String login, String password) {
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(AUTH_KEY_LOGIN, login);
        editor.putString(AUTH_KEY_PASSWORD, password);

        editor.apply();
    }

    public void putIsSaveLoginData(boolean isSave) {
        preferences.edit().putBoolean(AUTH_KEY_IS_SAVE_LOGIN_DATA, isSave).apply();
    }

    public LoginData getLoginData() {
        return new LoginData(getLogin(), getPassword());
    }

    public String getLogin() {
        return preferences.getString(AUTH_KEY_LOGIN, null);
    }

    public String getPassword() {
        return preferences.getString(AUTH_KEY_PASSWORD, null);
    }
}
