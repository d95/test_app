package com.meral.crmsystem.activitis.launch.auth;

import com.meral.crmsystem.data.DataManager;
import com.meral.crmsystem.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;


@Module
public class AuthFragmentModule {

    @Provides
    AuthViewModel provideAboutViewModel(DataManager dataManager,
                                         SchedulerProvider schedulerProvider) {
        return new AuthViewModel(dataManager, schedulerProvider);
    }
}
