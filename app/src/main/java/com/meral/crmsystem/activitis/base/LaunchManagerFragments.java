package com.meral.crmsystem.activitis.base;


import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LaunchManagerFragments extends ManagerFragments{

    @Inject
    public LaunchManagerFragments() {
        super();
    }

    public void openLoginFragment(BaseActivity activity) {
        replaceFragment(activity, LOGIN_FRAGMENT);
    }

    public void openAddAcountFragment(BaseActivity activity) {
        replaceFragment(activity, LOGIN_ADD_ACCOUNT_FRAGMENT);
    }

    public void openReportErrorFragment(BaseActivity activity) {
        replaceFragment(activity, LOGIN_ERROR_FRAGMENT);
    }
}
