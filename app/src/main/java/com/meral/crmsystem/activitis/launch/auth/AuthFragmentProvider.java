package com.meral.crmsystem.activitis.launch.auth;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class AuthFragmentProvider {

    @ContributesAndroidInjector(modules = AuthFragmentModule.class)
    abstract AuthFragment provideAboutFragmentFactory();
}
