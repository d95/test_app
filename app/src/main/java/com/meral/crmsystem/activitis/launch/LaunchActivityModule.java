package com.meral.crmsystem.activitis.launch;


import com.meral.crmsystem.activitis.base.LaunchManagerFragments;
import com.meral.crmsystem.activitis.base.ManagerFragments;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LaunchActivityModule {

    @Provides
    @Singleton
    ManagerFragments provideManager() {
        return new LaunchManagerFragments();
    }
}
