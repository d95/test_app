package com.meral.crmsystem.activitis.base;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.meral.crmsystem.R;
import com.meral.crmsystem.activitis.launch.auth.AuthFragment;

import java.util.ArrayList;
import java.util.List;


public class ManagerFragments {
    protected static final int LOGIN_FRAGMENT = 1;
    protected static final int LOGIN_ADD_ACCOUNT_FRAGMENT = 2;
    protected static final int LOGIN_ERROR_FRAGMENT = 3;

    private List<Integer> listFragments = null;


    public ManagerFragments() {
        this.listFragments = new ArrayList<>();
    }

    public boolean onBackPressed(BaseActivity activity) {
        if (listFragments.size() > 1) {
            listFragments.remove(listFragments.size() - 1);
            Integer newFragment = listFragments.get(listFragments.size() - 1);

            popFragment(newFragment);
            listFragments.add(newFragment);

            Fragment fragment = getFragment(newFragment);
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.containerViews, fragment).commit();

            logi("onBackPressed true " + listFragments);
            return true;
        }

        logi("onBackPressed false " + listFragments);
        return false;
    }

    protected void replaceFragment(BaseActivity activity, Integer newFragment) {
        logi("newFragment " + newFragment);
        logi("replaceFragment " + listFragments);

        popFragment(newFragment);
        listFragments.add(newFragment);

        Fragment fragment = getFragment(newFragment);

        activity.getSupportFragmentManager().beginTransaction().replace(R.id.containerViews, fragment).commit();

        logi("replaceFragment end " + listFragments);
    }

    private void popFragment(Integer fragment) {
        if (listFragments.contains(fragment)) listFragments.remove(fragment);

        logi("popFragment " + listFragments);
    }

    private Fragment getFragment(int index) {
        switch (index) {
            case LOGIN_FRAGMENT:
                return new AuthFragment();

            case LOGIN_ADD_ACCOUNT_FRAGMENT:
                return null;

            case LOGIN_ERROR_FRAGMENT:
                return null;

            default:
                return null;
        }
    }

        private void logi(String text) {
        Log.i("Esfer0", this.getClass().getSimpleName() + " " + text);
    }
}
