package com.meral.crmsystem.activitis.base;


import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.util.Log;


import com.meral.crmsystem.Const;
import com.meral.crmsystem.data.DataManager;
import com.meral.crmsystem.utils.rx.SchedulerProvider;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel<N> extends ViewModel {
    private N mNavigator;
    private final DataManager dataManager;
    private final SchedulerProvider schedulerProvider;
    private final ObservableBoolean isLoading = new ObservableBoolean(false);

    private CompositeDisposable compositeDisposable;

    public BaseViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        this.dataManager = dataManager;
        this.schedulerProvider = schedulerProvider;
    }

    public N getNavigator() {
        return mNavigator;
    }

    public void setNavigator(N mNavigator) {
        this.mNavigator = mNavigator;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public SchedulerProvider getSchedulerProvider() {
        return schedulerProvider;
    }

    public ObservableBoolean getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading.set(isLoading);
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        super.onCleared();
    }

    protected void logi(String text) {
        Log.i(Const.LOG.TAG, this.getClass().getSimpleName() + " " + text);
    }
}
