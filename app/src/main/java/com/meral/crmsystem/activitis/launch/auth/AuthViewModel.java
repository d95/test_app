package com.meral.crmsystem.activitis.launch.auth;


import android.databinding.ObservableBoolean;

import com.meral.crmsystem.activitis.base.BaseViewModel;
import com.meral.crmsystem.data.DataManager;
import com.meral.crmsystem.utils.CommonUtils;
import com.meral.crmsystem.utils.rx.SchedulerProvider;

public class AuthViewModel extends BaseViewModel <AuthNavigator> {
    private final ObservableBoolean isSaveMe = new ObservableBoolean(false);

    public ObservableBoolean getIsSaveMe() {
        return isSaveMe;
    }

    public AuthViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void onLoginClick() {
        logi("onLoginClick" + getIsSaveMe().get());
        getNavigator().login();
    }

    public void onAddAcountClick() {
        getNavigator().addAccount();
    }

    public void onReportOnErrorClick() {
        getNavigator().reportOnError();
    }

    public void login(String login, String password) {
        setIsLoading(true);
    }

    public boolean isEmailAndPasswordValid(String email, String password) {
        if (email == null || email.isEmpty()) {
            return false;
        }

        if (!CommonUtils.isEmailValid(email)) {
            return false;
        }

        if (password == null || password.isEmpty()) {
            return false;
        }

        return true;
    }
}
