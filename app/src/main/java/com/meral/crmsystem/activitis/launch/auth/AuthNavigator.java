package com.meral.crmsystem.activitis.launch.auth;


public interface AuthNavigator {

    void openMainActivity();

    void handleError(Throwable throwable);

    void login();

    void addAccount();

    void reportOnError();
}
