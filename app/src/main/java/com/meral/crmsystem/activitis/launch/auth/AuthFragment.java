package com.meral.crmsystem.activitis.launch.auth;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;

import com.meral.crmsystem.BR;
import com.meral.crmsystem.R;
import com.meral.crmsystem.activitis.base.BaseActivity;
import com.meral.crmsystem.activitis.base.BaseFragment;
import com.meral.crmsystem.databinding.FragmentAuthBinding;

import javax.inject.Inject;

public class AuthFragment extends BaseFragment<FragmentAuthBinding, AuthViewModel> implements AuthNavigator {

    @Inject
    AuthViewModel authViewModel;

    FragmentAuthBinding fragmentAuthBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        hideActionBar();
        fragmentAuthBinding = getViewDataBinding();
        authViewModel.setNavigator(this);
    }

    private void hideActionBar() {
        ActionBar actionBar =  ((BaseActivity) getActivity()).getSupportActionBar();

        if (actionBar == null) return;

        actionBar.hide();
    }

    @Override
    public AuthViewModel getViewModel() {
        logi("getViewModel^ " + authViewModel);
        return authViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_auth;
    }

    @Override
    public void openMainActivity() {
        showMsg("openMainActivity");
    }

    @Override
    public void handleError(Throwable throwable) {
        showMsg("handleError");
    }

    @Override
    public void login() {
        String email = fragmentAuthBinding.loginEditText.getText().toString();
        String password = fragmentAuthBinding.passwordEditText.getText().toString();
        if (authViewModel.isEmailAndPasswordValid(email, password)) {
            hideKeyboard();
            authViewModel.login(email, password);
        } else {
            showMsg(getString(R.string.invalid_email_password));
        }
    }

    @Override
    public void addAccount() {
        authViewModel.setIsLoading(false);
    }

    @Override
    public void reportOnError() {
        showMsg(" reportOnError");
    }
}
