package com.meral.crmsystem.activitis.launch;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.meral.crmsystem.R;
import com.meral.crmsystem.activitis.base.BaseActivity;
import com.meral.crmsystem.activitis.base.LaunchManagerFragments;
import com.meral.crmsystem.activitis.base.ManagerFragments;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class LaunchActivity extends BaseActivity implements HasSupportFragmentInjector {

    @Inject
    LaunchManagerFragments launchManagerFragments;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        launchManagerFragments.openLoginFragment(this);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

}


