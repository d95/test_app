package com.meral.crmsystem.core.di;


import android.app.Application;

import com.meral.crmsystem.core.AgrApp;
import com.meral.crmsystem.core.di.modules.ActivityBuilder;
import com.meral.crmsystem.core.di.modules.AppModule;
import com.meral.crmsystem.core.di.modules.DbModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, DbModule.class, ActivityBuilder.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(AgrApp app);
}
