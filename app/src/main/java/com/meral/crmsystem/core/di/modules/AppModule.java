package com.meral.crmsystem.core.di.modules;


import android.app.Application;
import android.content.Context;

import com.meral.crmsystem.data.AppDataManager;
import com.meral.crmsystem.data.DataManager;
import com.meral.crmsystem.utils.pref.AppPreferences;
import com.meral.crmsystem.utils.pref.AppPreferencesHelper;
import com.meral.crmsystem.utils.pref.AuthPreferences;
import com.meral.crmsystem.utils.pref.AuthPreferencesHelper;
import com.meral.crmsystem.utils.rx.AppSchedulerProvider;
import com.meral.crmsystem.utils.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    SchedulerProvider provideSchedulerProvidep() {
        return new AppSchedulerProvider();
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    AppPreferencesHelper provideAppPreferencesHelper(AppPreferences appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    AuthPreferencesHelper provideAuthPreferencesHelper(AuthPreferences authPreferences) {
        return authPreferences;
    }
}
