package com.meral.crmsystem.core.di.modules;

import com.meral.crmsystem.activitis.launch.LaunchActivity;
import com.meral.crmsystem.activitis.launch.auth.AuthFragmentProvider;
import com.meral.crmsystem.activitis.launch.LaunchActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {LaunchActivityModule.class, AuthFragmentProvider.class})
    abstract LaunchActivity bindLaunchActivity();

}

